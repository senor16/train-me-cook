-- Debug with EmmyLua
package.cpath = package.cpath .. ';/home/senor16/.local/share/JetBrains/IntelliJIdea2021.2/EmmyLua/classes/debugger/emmy/linux/?.so'
local dbg = require('emmy_core')
dbg.tcpConnect('localhost', 9966)
-- Initialise random seed
math.randomseed(os.time())
-- Load fonts
local font20 = love.graphics.newFont("Fonts/Freebooter Italic.ttf", 20)
local font25 = love.graphics.newFont("Fonts/Freebooter Italic.ttf", 25)
local font30 = love.graphics.newFont("Fonts/Freebooter Italic.ttf", 30)
local font50 = love.graphics.newFont("Fonts/Freebooter Italic.ttf", 50)
local fontPuzzle = love.graphics.newFont("Fonts/WWFreebie.ttf", 20)
-- Initials values
love.window.setTitle("Train me Cook")
local screen = {}
local suspend = true
local showAbout = false
local gamecomplete = false
local showRules = false
local showStart = false
local startGame = false
local score = 0
local smoney = 20
local selectLanguage = true
local language = "fr"
local showVict = false
local puzzleMoves = 0
local levelFail = false
local time = 0
local moving = false
local Puzzle = {}
local Previous = {}
local Rectangle = {}
local imageBlocGot = 0
local generationList = { 0, 1, 2, 3, "a" }
local Image = {}
local level = 1
local chapter = 1
local wait = {
  mainMenu = 0,
  start = 0
}
-- Initiate GUI
local myGui = require("gcgui")
-- About
local aboutGroup = nil
local aboutContent = nil
local aboutPanel = nil
local aboutTitle = nil
-- Rules
local rulesGroup = nil
local rulesTitle = nil
local rulesContent = nil
-- Language buttons
local languageGroup = nil
local languageLabel = nil
local languageEnBtn = nil
local languageFrBtn = nil
-- Main screen
local mainScreenGroup = nil
local title = nil
local btnNewgame = nil
local btnRules = nil
local btnAbout = nil
-- Congrats screen
local congratsGroup = nil
local congratsText = nil
local congratsBtn = nil
-- Failure screen
local failureGroup = nil
local failureText = nil
local failureTextCost = nil
local failureBtnRetry = nil
local failureBtnHome = nil
--- Menu bar
-- Menu File
local menuFile = nil
local menuFileItemHome = nil
local menuFileItemExit = nil
-- Menu About
local menuAbout = nil
local menuAboutItemRules = nil
local menuAboutItemAbout = nil
-- Show the traces
io.stdout:setvbuf("no")
--Empèche Love de filtrer les contours des images quand elles sont redimentionnées
-- Indispensable pour du pxel art
love.graphics.setDefaultFilter("nearest")
love.graphics.setBackgroundColor(126 / 255, 99 / 255, 49 / 255, 191 / 255)
Image = {}

-- Tweening functions
function easeInSin(t, b, c, d)
  return -c * math.cos(t / d * (math.pi / 2)) + c + b
end

function easeOutSin(t, b, c, d)
  return c * math.sin(t / d * (math.pi / 2)) + b
end

function easeInExpo(t, b, c, d)
  return c * math.pow(2, 10 * (t / d - 1)) + b
end

function easeOutExpo(t, b, c, d)
  return c * (-math.pow(2, -10 * t / d) + 1) + b
end

---@param pValue number
---@param pDistance number
---@param pDuration number
function InitTween(pValue, pDistance, pDuration)
  local tweening = {}
  tweening.time = 0
  tweening.value = pValue
  tweening.distance = pDistance
  tweening.duration = pDuration
  return tweening
end

function UpdateTween(pTween, dt)
  if pTween.time < pTween.duration then
    pTween.time = pTween.time + dt
  end
end

function DrawRectangle()
  love.graphics.setColor(1, 1, 1, 1)
  love.graphics.rectangle("fill", Rectangle.x, Rectangle.y + 40, 240, 120)
  love.graphics.setColor(128 / 255, 108 / 255, 0, 1)
  love.graphics.rectangle("fill", Rectangle.x + 3, Rectangle.y + 43, 236, 116)
  love.graphics.setColor(1, 1, 1, 1)
end

function CheckVictory()
  vict = false
  if chapter == 1 then
    if imageBlocGot == 4 then
      suspend = true
      vict = true
      showVict = true
    end
  elseif chapter == 2 then
    if imageBlocGot == 9 then
      vict = true
      suspend = true
      showVict = true
    end
  end
  return vict
end

function GetLevel()
  if chapter == 1 then
    puzzleMoves = 50
    Image = {
      { "a", "a" },
      { "a", "a" }
    }
    if level == 1 then
      Puzzle = {
        { 1, 2, 0, 3, 2, 1 },
        { 1, 1, 2, 0, 3, 2 },
        { 2, 2, 3, 0, 1, 1 },
        { 0, 0, 3, 3, 2, 2 },
        { 2, 0, 1, 0, 1, 1 },
        { 3, 1, 2, 1, 0, 0 },
        { 0, 0, 3, 3, 1, 2 }
      }
    elseif level == 2 then
      Puzzle = {
        { 3, 2, 1, 3, 2, 1 },
        { 1, 3, 0, 0, 3, 2 },
        { 2, 2, 3, 0, 1, 1 },
        { 0, 1, 2, 3, 2, 2 },
        { 2, 0, 1, 0, 1, 1 },
        { 3, 1, 2, 2, 0, 0 },
        { 0, 0, 3, 2, 1, 1 }
      }
    end
  elseif chapter == 2 then
    puzzleMoves = 100
    Image = {
      { "a", "a", "a" },
      { "a", "a", "a" },
      { "a", "a", "a" }
    }
    if level == 1 then
      Puzzle = {
        { 1, 2, 0, 3, 2, 1 },
        { 1, 1, 2, 2, 3, 0 },
        { 2, 2, 3, 1, 1, 0 },
        { 0, 1, 3, 3, 2, 2 },
        { 2, 3, 1, 0, 1, 1 },
        { 3, 1, 3, 1, 0, 0 },
        { 1, 0, 3, 3, 1, 1 }
      }
    elseif level == 2 then
      Puzzle = {
        { 1, 2, 0, 3, 2, 1 },
        { 1, 1, 2, 1, 3, 2 },
        { 2, 2, 3, 2, 0, 1 },
        { 0, 2, 1, 3, 2, 2 },
        { 2, 0, 1, 0, 1, 0 },
        { 3, 1, 2, 1, 0, 0 },
        { 0, 0, 3, 3, 1, 1 }
      }
    elseif level == 3 then
      Puzzle = {
        { 1, 2, 0, 3, 2, 1 },
        { 1, 1, 2, 0, 3, 2 },
        { 2, 0, 1, 2, 0, 1 },
        { 0, 2, 1, 3, 2, 2 },
        { 2, 0, 1, 0, 1, 0 },
        { 3, 1, 2, 1, 0, 0 },
        { 0, 0, 3, 0, 1, 1 }
      }
    elseif level == 4 then
      Puzzle = {
        { 1, 2, 0, 3, 2, 1 },
        { 1, 1, 3, 1, 3, 2 },
        { 2, 2, 3, 0, 0, 1 },
        { 0, 2, 1, 3, 2, 3 },
        { 2, 1, 0, 0, 1, 0 },
        { 3, 1, 2, 1, 0, 1 },
        { 0, 0, 3, 3, 1, 1 }
      }
    end
  end
end

function BlockNumberInThePuzze()
  local num = 0
  for l = 1, #Puzzle do
    for c = 1, #Puzzle[1] do
      if Puzzle[l][c] == "a" then
        num = num + 1
      end
    end
  end
  return num
end

function getTargetNumberBloc()
  local b
  if chapter == 1 then
    b = 4
  elseif chapter == 2 then
    b = 9
  end
  return b
end

function DrawRules()
  if showRules then
    if rulesTitle == nil then
      rulesGroup = myGui.newGroup(0, 15)
      rulesTitle = myGui.newLabel(0, 0, "Train me Cook", font50, "center", "center", screen.width - 50)
      rulesGroup:addElement(rulesTitle)
      local x
      if language == "en" then
        x = 20
      else
        x = 10
      end
      rulesContent = myGui.newLabel(x, 70, "", font25, "center", "center")
      rulesGroup:addElement(rulesContent)
      rulesGroup:setVisible(true)
    end

    if language == "en" then
      rules = "\tPut three blocks of same form to get them desapear.\n"
      rules = rules .. "\tAt each level, you will have to collect food ingredients.\n"
      rules = rules .. "After collecting the requested numer of those food ingredients,\n"
      rules = rules .. "you will be able to cook, so you could complete the current\n"
      rules = rules .. " level and access the next level.\n"
      rules = rules .. "\tYou have a fixed amount of moves that you can perform\n"
      rules = rules .. "If you use your amount of moves before collecting the ingredients,\n"
      rules = rules .. "you will pay to buy them. \n"
      rules = rules .. "\tYou lose the game when you are no longer able to buy ingredients\n"
      rules = rules .. "after using all your amount of moves"
    elseif language == "fr" then
      rules = "\tMet trois blocks ayant la même forme pour les faire disparaître.\n"
      rules = rules .. "\tA chaque niveau, tu dois collecter un certains nombre d'ingredients.\n"
      rules = rules .. "Une fois les ingrédients collectés, tu sera en mésure de cuisiner\n"
      rules = rules .. "et ainsi passer au niveau suivant.\n"
      rules = rules .. "\tTu as un nombre limité de mouvement. Si tu épuise le nombre\n"
      rules = rules .. "de mouvements que tu peux faire aveant d'avoir collecté les ingrédients,\n"
      rules = rules .. "tu devra payer pour en acheter.\n"
      rules = rules .. "\tTu perd la partie lorsque tu n'aplus de quoi acheter des ingrédients\n"
    end
    rulesContent.text = rules
    rulesGroup:draw()
  end
end

function DrawAbout()
  if showAbout then
    suspend = true
    if aboutTitle == nil then
      aboutGroup = myGui.newGroup(150, 100)
      aboutTitle = myGui.newLabel(0, 0, "Train me Cook", font50, "center", "center", 280, 60)
      aboutGroup:addElement(aboutTitle)
    end
    aboutPanel = myGui.newPanel(0, 0, 290, 200)
    aboutGroup:addElement(aboutPanel)
    if aboutContent == nil then
      aboutContent = myGui.newLabel(0, 60, "", font25, "center", "bottom", 40, 60)
      aboutGroup:addElement(aboutContent)
    end
    aboutGroup:setVisible(true)
    if language == "en" then
      aboutContent.text = "Author : Sesso Kosga\n\t\t\tBamokai Michée"
    elseif language == "fr" then
      aboutContent.text = "Auteur : Sesso Kosga\n\t\t\tBamokai Michée"
    end
    aboutContent.text = aboutContent.text .. "\ne-mail: kosgasesso@gmail.com"
    aboutGroup:draw()
  end
end

function DrawMenu()
  love.graphics.setFont(font20)
  if language == "en" then
    menuFile.parent.label:setText("File")
    menuFileItemHome.label:setText("Home")
    menuFileItemExit.label:setText("Exit")

    menuAbout.parent.label:setText("About")
    menuAboutItemRules.label:setText("Rules")
  elseif language == "fr" then
    menuFile.parent.label:setText("Fichier")
    menuFileItemHome.label:setText("Accueil")
    menuFileItemExit.label:setText("Quitter")

    menuAbout.parent.label:setText("À propos")
    menuAboutItemRules.label:setText("Règles")
  end
  menuAboutItemAbout.label:setText("?")
  menuFile:draw()
  menuAbout:draw()

end

function AddBlocks()
  for c = 1, #Puzzle do
    if Puzzle[1][c] == "" then
      if imageBlocGot + BlockNumberInThePuzze() < getTargetNumberBloc() and BlockNumberInThePuzze() < 2 then
        Puzzle[1][c] = generationList[math.random(1, 5)]
      else
        Puzzle[1][c] = generationList[math.random(1, 4)]
      end
    end
  end
end

function getReward()
  for c = 1, 6 do
    if Puzzle[#Puzzle][c] == "a" then
      Puzzle[#Puzzle][c] = ""
      imageBlocGot = imageBlocGot + 1
    end
  end
end

function RearrangedPuzzles()
  local clean = true
  for l = #Puzzle, 2, -1 do
    for c = 1, #Puzzle[l] do
      if Puzzle[l][c] == "" then
        Puzzle[l][c] = Puzzle[l - 1][c]
        Puzzle[l - 1][c] = ""
        clean = false
      end
    end
  end
  return clean
end

function DestructPuzzle()
  local win = false
  for l = 1, #Puzzle do
    for c = 1, #Puzzle[l] - 2 do
      if Puzzle[l][c] == 0 and Puzzle[l][c + 1] == 0 and Puzzle[l][c + 2] == 0 then
        Puzzle[l][c] = ""
        Puzzle[l][c + 1] = ""
        Puzzle[l][c + 2] = ""
        m = c + 3
        while m < 7 and Puzzle[l][m] == 0 do
          Puzzle[l][m] = ""
          m = m + 1
        end
        score = score + m * 5
        win = true
      end

      if Puzzle[l][c] == 1 and Puzzle[l][c + 1] == 1 and Puzzle[l][c + 2] == 1 then
        Puzzle[l][c] = ""
        Puzzle[l][c + 1] = ""
        Puzzle[l][c + 2] = ""

        m = c + 3
        while m < 7 and Puzzle[l][m] == 1 do
          Puzzle[l][m] = ""
          m = m + 1
        end
        score = score + m * 5
        win = true
      end

      if Puzzle[l][c] == 2 and Puzzle[l][c + 1] == 2 and Puzzle[l][c + 2] == 2 then
        Puzzle[l][c] = ""
        Puzzle[l][c + 1] = ""
        Puzzle[l][c + 2] = ""

        m = c + 3
        while m < 7 and Puzzle[l][m] == 2 do
          Puzzle[l][m] = ""
          m = m + 1
        end
        score = score + m * 5
        win = true
      end

      if Puzzle[l][c] == 3 and Puzzle[l][c + 1] == 3 and Puzzle[l][c + 2] == 3 then
        Puzzle[l][c] = ""
        Puzzle[l][c + 1] = ""
        Puzzle[l][c + 2] = ""

        m = c + 3
        while m < 7 and Puzzle[l][m] == 3 do
          Puzzle[l][m] = ""
          m = m + 1
        end
        score = score + m * 5
        win = true
      end
    end
  end

  for c = 1, 6 do
    for l = 1, 5 do
      if Puzzle[l][c] == 0 and Puzzle[l + 1][c] == 0 and Puzzle[l + 2][c] == 0 then
        Puzzle[l][c] = ""
        Puzzle[l + 1][c] = ""
        Puzzle[l + 2][c] = ""

        m = l + 3
        while m < 8 and Puzzle[m][c] == 0 do
          Puzzle[m][c] = ""
          m = m + 1
        end
        score = score + m * 5
        win = true
      end

      if Puzzle[l][c] == 1 and Puzzle[l + 1][c] == 1 and Puzzle[l + 2][c] == 1 then
        Puzzle[l][c] = ""
        Puzzle[l + 1][c] = ""
        Puzzle[l + 2][c] = ""

        m = l + 3
        while m < 8 and Puzzle[m][c] == 1 do
          Puzzle[m][c] = ""
          m = m + 1
        end
        score = score + m * 5
        win = true
      end

      if Puzzle[l][c] == 2 and Puzzle[l + 1][c] == 2 and Puzzle[l + 2][c] == 2 then
        Puzzle[l][c] = ""
        Puzzle[l + 1][c] = ""
        Puzzle[l + 2][c] = ""

        m = l + 3
        while m < 8 and Puzzle[m][c] == 2 do
          Puzzle[m][c] = ""
          m = m + 1
        end
        score = score + m * 5
        win = true
      end

      if Puzzle[l][c] == 3 and Puzzle[l + 1][c] == 3 and Puzzle[l + 2][c] == 3 then
        Puzzle[l][c] = ""
        Puzzle[l + 1][c] = ""
        Puzzle[l + 2][c] = ""

        m = l + 3
        while m < 8 and Puzzle[m][c] == 3 do
          Puzzle[m][c] = ""
          m = m + 1
        end
        score = score + m * 5
        win = true
      end
    end
  end

  return win
end

function DrawPuzzle()
  y = 150
  love.graphics.setColor(1, 1, 1, 230 / 255)
  love.graphics.rectangle("fill", Rectangle.x, Rectangle.y, Rectangle.width, Rectangle.height)
  love.graphics.setColor(128 / 255, 108 / 255, 0, 230 / 255)
  for l = 1, 7 do
    x = 150
    for c = 1, 6 do
      love.graphics.rectangle("fill", x, y, 40 - 2, 40 - 2)
      x = x + 40
    end
    y = y + 40
  end
  love.graphics.setColor(1, 1, 1, 230 / 255)
  y = 160
  for i = 1, #Puzzle do
    x = 163
    for j = 1, #Puzzle[1] do
      if Puzzle[i][j] == 0 then
        love.graphics.print("e", x, y)
      else
        love.graphics.print(Puzzle[i][j], x, y)
      end
      x = x + 40
    end
    y = y + 40
  end
end

function GetImgRow()
  if imageBlocGot > 0 and imageBlocGot < 4 then
    return 1
  elseif imageBlocGot > 3 and imageBlocGot < 7 then
    return 2
  else
    return 3
  end
end

function DrawImage()
  love.graphics.setColor(1, 1, 1, 230 / 255)
  local col
  if chapter == 1 then
    col = 2
  elseif chapter == 2 then
    col = 3
  end

  love.graphics.rectangle("fill", 10, 150, 40 * col + 2, 40 * col + 2)
  y = 152
  love.graphics.setColor(128 / 255, 108 / 255, 0, 230 / 255)
  for l = 1, col do
    x = 12
    for c = 1, col do
      love.graphics.rectangle("fill", x, y, 40 - 2, 40 - 2)
      x = x + 40
    end
    y = y + 40
  end
  if imageBlocGot > 0 then
    love.graphics.setColor(1, 1, 1, 1)
    y = 125 + 40 * col
    local ip = 0
    for i = 1, #Image do
      x = 25
      for j = 1, #Image[i] do
        love.graphics.print(Image[i][j], x, y)
        ip = ip + 1
        x = x + 40
        if ip >= imageBlocGot then
          break
        end
      end
      if ip >= imageBlocGot then
        break
      end
      y = y - 40
    end
  end
end
function InitTexts()

end
function love.load()
  iconeImg = love.image.newImageData("icone.png")
  love.window.setIcon(iconeImg)
  screen.width = 600
  screen.height = 450
  love.window.setMode(screen.width, screen.height)

  --- Language buttons
  languageGroup = myGui.newGroup(0, 130)
  languageGroup.ease = easeOutExpo
  languageGroup.tween = InitTween(-(screen.width - 100) / 2, (screen.width - 100), .8)
  languageLabel = myGui.newLabel(-30, 0, "Choose a language", font30)
  languageEnBtn = myGui.newButton(0, 40, 100, 30, "English", font25)
  languageFrBtn = myGui.newButton(0, 80, 100, 30, "Français", font25)
  -- events
  function onBtnFrReleased()
    mainScreenGroup.tween = InitTween(-(screen.width - 170) / 2, (screen.width - 170), .8)
    mainScreenGroup.ease = easeOutExpo
    languageGroup.tween = InitTween((screen.width - 100) / 2, (screen.width - 100), .8)
    languageGroup.ease = easeInExpo
    language = "fr"
    btnNewgame.label.text = "Nouveau Jeu"
    btnRules.label.text = "Règles"
    btnAbout.label.text = "Auteur"
    selectLanguage = true
    showStart = true
    wait.mainMenu = .8
  end

  function onBtnEnReleased()
    mainScreenGroup.tween = InitTween(-(screen.width - 170) / 2, (screen.width - 170), .8)
    mainScreenGroup.ease = easeOutExpo
    languageGroup.tween = InitTween((screen.width - 100) / 2, (screen.width - 100), .8)
    languageGroup.ease = easeInExpo
    language = "en"
    btnNewgame.label.text = "New Game"
    btnRules.label.text = "Rules"
    btnAbout.label.text = "Author"
    selectLanguage = true
    showStart = true
    wait.mainMenu = .8
  end
  languageEnBtn:setEvent("released", onBtnEnReleased)
  languageFrBtn:setEvent("released", onBtnFrReleased)

  languageGroup:addElement(languageLabel)
  languageGroup:addElement(languageFrBtn)
  languageGroup:addElement(languageEnBtn)
  languageGroup:setVisible(true)

  --- Main screen
  love.graphics.setFont(font50)
  love.graphics.print("Train me Cook", 170, 50)
  love.graphics.setFont(font30)
  local ng, ru, au
  if language == "en" then
    ng = "New Game"
    ru = "Rules"
    au = "Author"
  elseif language == "fr" then
    ng = "Nouveau Jeu"
    ru = "Règles"
    au = "Auteur"
  end
  mainScreenGroup = myGui.newGroup(-(screen.width - 170) / 2, 50)
  title = myGui.newLabel(-30, 0, "Train me Cook", font50, "center", "center")
  btnNewgame = myGui.newButton(0, 70, 170, 40, ng, font30)
  btnRules = myGui.newButton(0, 120, 170, 40, ru, font30)
  btnAbout = myGui.newButton(0, 170, 170, 40, au, font30)
  -- events
  function onBtnNewgameReleased()
    startGame = true
    showStart = true
    suspend = false
    mainScreenGroup.ease = easeInExpo
    mainScreenGroup.tween = InitTween((screen.width - 170) / 2, screen.width, .8)
    wait.start = .8
  end
  function onBtnRulesReleased()
    showRules = true
    showStart = true
    mainScreenGroup.ease = easeInExpo
    mainScreenGroup.tween = InitTween((screen.width - 170) / 2, screen.width, .8)
    wait.start = .8
  end
  function onBtnAboutReleased()
    showStart = true
    showAbout = true
    mainScreenGroup.ease = easeInExpo
    mainScreenGroup.tween = InitTween((screen.width - 170) / 2, screen.width, .8)
    wait.start = .8
  end

  btnNewgame:setEvent("released", onBtnNewgameReleased)
  btnRules:setEvent("released", onBtnRulesReleased)
  btnAbout:setEvent("released", onBtnAboutReleased)
  -- add elements to the parent gui
  mainScreenGroup:addElement(title)
  mainScreenGroup:addElement(btnNewgame)
  mainScreenGroup:addElement(btnRules)
  mainScreenGroup:addElement(btnAbout)
  mainScreenGroup:setVisible(true)

  ---  Congrats screen
  congratsGroup = myGui.newGroup(175, 200)
  congratsText = myGui.newLabel(0, 0, "", font30, "center")
  congratsBtn = myGui.newButton(30, 70, 120, 34, "next", font30)
  -- event
  function onCongratsBtnReleased()
    showVict = false
    suspend = false
    if chapter == 1 then
      if imageBlocGot == 4 then
        level = level + 1
        imageBlocGot = 0
        if level > 2 then
          level = 1
          chapter = 2
        end
        GetLevel()
      end
    elseif chapter == 2 then
      if imageBlocGot == 9 then
        level = level + 1
        imageBlocGot = 0
        if level > 4 then
          suspend = true
        end
        GetLevel()
      end
    end
  end
  congratsBtn:setEvent("released", onCongratsBtnReleased)
  congratsGroup:addElement(congratsBtn)
  congratsGroup:addElement(congratsText)
  congratsGroup:setVisible(true)
  --- Failure screen
  failureGroup = myGui.newGroup(175, 200)
  failureText = myGui.newLabel(0, 0, "", font30, "center", "center", 180, 30)
  failureTextCost = myGui.newLabel(10, 75, "", font20)
  failureBtnRetry = myGui.newButton(0, 40, 100, 30, "", font25)
  failureBtnHome = myGui.newButton(110, 40, 80, 30, "", font25)
  -- events
  function onBtnRestartReleased()
    if smoney >= 5 then
      smoney = smoney - 5
    end
    suspend = false
    levelFail = false
    GetLevel()
  end

  function onBtnHomeReleased()
    showStart = true
    smoney = 20
    suspend = false
    levelFail = false
    GetLevel()
  end

  failureBtnHome:setEvent("released", onBtnHomeReleased)
  failureBtnRetry:setEvent("released", onBtnRestartReleased)
  failureGroup:addElement(failureBtnHome)
  failureGroup:addElement(failureBtnRetry)
  failureGroup:addElement(failureText)
  failureGroup:addElement(failureTextCost)
  failureGroup:setVisible(true)
  --- Menu Bar
  -- Menu File
  menuFile = myGui.newMenu(5, 3, 50, 25, "", font20, 58, 80)
  menuFileItemHome = myGui.newButton(8, 30, 50, 25, "", font20, false)
  menuFileItemExit = myGui.newButton(8, 55, 50, 25, "", font20, false)
  -- events
  function onMenuItemExitReleased()
    love.event.quit()
  end
  function onMenuItemHomeReleased()
    mainScreenGroup.tween = InitTween(-(screen.width - 170) / 2, (screen.width - 170), .8)
    mainScreenGroup.ease = easeOutExpo
    showStart = true
    showAbout = false
    showRules = false
    suspend = false
    menuFile:setVisible(false)
  end
  menuFileItemHome:setEvent("released", onMenuItemHomeReleased)
  menuFileItemExit:setEvent("released", onMenuItemExitReleased)
  menuFile:addElement(menuFileItemHome)
  menuFile:addElement(menuFileItemExit)
  -- Menu About
  menuAbout = myGui.newMenu(40, 3, 60, 25, "", font20, 80, 80)
  menuAboutItemAbout = myGui.newButton(40, 30, 50, 25, "", font20, false)
  menuAboutItemRules = myGui.newButton(40, 55, 50, 25, "", font20, false)
  -- events
  function onMenuItemRulesReleased()
    showRules = true
    showAbout = false
    showStart = false
    menuAbout:setVisible(false)
  end
  function onMenuItemAboutReleased()
    showStart = false
    showRules = false
    showAbout = true
    menuAbout:setVisible(false)
  end
  menuAboutItemAbout:setEvent("released", onMenuItemAboutReleased)
  menuAboutItemRules:setEvent("released", onMenuItemRulesReleased)
  menuAbout:addElement(menuAboutItemAbout)
  menuAbout:addElement(menuAboutItemRules)

  Rectangle.x = 148.5
  Rectangle.y = 148.5
  Rectangle.width = 40 * 6 + 2
  Rectangle.height = 40 * 7 + 2
  GetLevel()
end

function love.draw()
  love.graphics.setFont(font20)
  if selectLanguage then
    languageGroup:draw()
  end

  if not showStart then
    DrawMenu()
    DrawAbout()
    DrawRules()
    if not showAbout and not showRules and startGame then
      love.graphics.print("Score : " .. score, 450, 200)
      if language == "en" then
        love.graphics.print("Collect  " .. getTargetNumberBloc(), 15, 150 + 42 * 3)
        love.graphics.print("Unit : " .. chapter, 15, 150 + 42 * 4)
        love.graphics.print("Lesson : " .. level, 15, 150 + 43 * 5)
        love.graphics.print("Moves : " .. puzzleMoves, 350, 46)
        love.graphics.print("Money : " .. smoney, 450, 46)
      elseif language == "fr" then
        love.graphics.print("Collecte " .. getTargetNumberBloc(), 15, 150 + 42 * 3)
        love.graphics.print("Unité : " .. chapter, 15, 150 + 42 * 4)
        love.graphics.print("Leçon : " .. level, 15, 150 + 43 * 5)
        love.graphics.print("Déplacements : " .. puzzleMoves, 300, 46)
        love.graphics.print("Argent : " .. smoney, 450, 46)
      end
      love.graphics.setFont(fontPuzzle)
      love.graphics.print("  a ", 70, 150 + 43 * 3)
      DrawImage()
      DrawPuzzle()
      if showVict and suspend then
        DrawRectangle()
        if level == 4 then
          gamecomplete = true
          if language == "en" then
            congratsText.text = " \tCongrats !\nYou completed \n\tthe game"
          elseif language == "fr" then
            congratsText.text = " \t\tFélicitation !\n\t Tu as terminé\n\t le jeu"
          end
          congratsBtn:setVisible(false)
        else
          congratsBtn:setVisible(true)
          if language == "en" then
            congratsText.text = " \t\tCongrats !\n Lesson completed "
            congratsBtn.label:setText("Next")

          elseif language == "fr" then
            congratsText.text = "\tFélicitation !\n\tLeçon terminé "
            congratsBtn.label:setText("Suivant")
          end
        end
        congratsGroup:draw()
      end

      if levelFail then
        failureBtnHome:setX(110)
        failureBtnRetry:setVisible(true)
        DrawRectangle()
        if smoney >= 5 then
          if language == "en" then
            failureText:setText("You Failed ")
            failureBtnRetry.label:setText("Try again")
            failureTextCost:setText("Money (-5)")
            failureBtnHome.label:setText("Home")
          elseif language == "fr" then
            failureText:setText("Vous avez perdu ")
            failureBtnRetry.label:setText("Réessayer")
            failureTextCost:setText("Argent (-5)")
            failureBtnHome.label:setText("Accueil")
          end
        else
          failureBtnRetry:setVisible(false)
          failureBtnHome:setX(50)
          if language == "en" then
            failureText:setText("Game Over")
            failureBtnHome.label:setText("Home")
          elseif language == "fr" then
            failureText:setText("Partie Terminé ")
            failureBtnHome.label:setText("Accueil")
          end
        end
        failureGroup:draw()
      end
    end
  else
    mainScreenGroup:draw()
  end

end

function love.mousepressed(xm, ym, b)
  if b == 1 then
    yp = math.floor((ym - 150) / 40) + 1
    xp = math.floor((xm - 150) / 40) + 1
    moving = true
    if xp >= 1 and xp <= 6 and yp <= 7 and yp >= 1 then
      Previous.value = Puzzle[yp][xp]
      Previous.l = yp
      Previous.c = xp
    end
  end
end

function love.mousereleased(xm, ym, b)
  moving = false
  yp = math.floor((ym - 150) / 40) + 1
  xp = math.floor((xm - 150) / 40) + 1
  if
  xp >= 1 and xp <= 6 and yp <= 7 and yp >= 1 and
    ((xp == Previous.c and (yp == Previous.l + 1 or yp == Previous.l - 1)) or
      yp == Previous.l and (xp == Previous.c + 1 or xp == Previous.c - 1))
  then
    Puzzle[Previous.l][Previous.c] = Puzzle[yp][xp]
    Puzzle[yp][xp] = Previous.value
    puzzleMoves = puzzleMoves - 1

    if not DestructPuzzle() then
      puzzleMoves = puzzleMoves + 1
      Previous.value = Puzzle[Previous.l][Previous.c]
      Puzzle[Previous.l][Previous.c] = Puzzle[yp][xp]
      Puzzle[yp][xp] = Previous.value
    end
  end
end

function love.update(dt)
  xm,
  ym = love.mouse.getPosition()
  font = love.graphics.getFont()
  if wait.mainMenu > 0 then
    wait.mainMenu = wait.mainMenu - dt
  else
    if showStart then
      selectLanguage = false
    end
  end

  if wait.start > 0 then
    wait.start = wait.start - dt
  else
    if startGame or showRules or showAbout then
      showStart = false
    end
  end

  if selectLanguage then
    UpdateTween(languageGroup.tween, dt)
    languageGroup.x = languageGroup.ease(languageGroup.tween.time, languageGroup.tween.value, languageGroup.tween.distance, languageGroup.tween.duration)
    languageGroup:update(dt)
  else
    if not suspend then
      CheckVictory()
      time = time + dt
      if time >= .2 then
        DestructPuzzle()
        RearrangedPuzzles()
        getReward()
        AddBlocks()
        time = time - .2
      end
    end

    if puzzleMoves == 0 and not CheckVictory() and RearrangedPuzzles() then
      suspend = true
      levelFail = true
    end

    menuFile:update(dt)
    menuAbout:update(dt)
    if suspend then
      congratsGroup:update(dt)
    end
    if suspend and not gamecomplete then
      failureGroup:update(dt)
    end
    if showStart and wait.mainMenu <= 0 then
      UpdateTween(mainScreenGroup.tween, dt)
      mainScreenGroup.x = mainScreenGroup.ease(mainScreenGroup.tween.time, mainScreenGroup.tween.value, mainScreenGroup.tween.distance, mainScreenGroup.tween.duration)
      mainScreenGroup:update(dt)
    end
  end


end

function love.keypressed(key)
  if key == "escape" then
    love.event.quit()
  end
end
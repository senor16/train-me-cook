# Train me Cook
![Main Screen](images/home.png)

This is a puzzle game which I made with love 2d and lua.

You have to put three blocks of same form to get them desapear. At each level you have to collect a requested number of food ingredients. After collecting them, you will access the next level.

There is a fixed number of moves you can perform. If you use your amount of moves without getting the requested number of ingredients, you fail the level and pay to buy thoses ingredients.

You lose the game when you are no longer able to buy ingredients after using all your amount of moves.

![A game party](images/game.png)
![When you fail](images/failed.png)
![When you succeed](images/win.png)

Framework : `Love 2d`   
Language : `Lua`  
Author : Sesso Kosga   
Email : kosgasesso@gmail.com 
function love.conf(t)
  local desc = ""
  desc = "This is a puzzle game which I made with love 2d and lua.\n You have to put three blocks of same form to get them desapear. At each level you have to collect a requested number of food ingredients. After collecting them, you will access the next level.\n There is a fixed number of moves you can perform. If you use your amount of moves without getting the requested number of ingredients, you fail the level and pay to buy thoses ingredients.\n You lose the game when you are no longer able to buy ingredients after using all your amount of moves. "
  t.releases = {
  title = "Train me Cook", -- The project title (string)
  package = nil, -- The project command and package name (string)
  loveVersion = "11.3", -- The project LÖVE version
  version = "2.0", -- The project version
  author = "Sesso Kosga", -- Your name (string)
  email = "kosgasesso@gmail.com", -- Your email (string),
  description=desc,
  homepage = "https: // senor16.itch.io / train - me - cook ", -- The project homepage (string)
  identifier = nil, -- The project Uniform Type Identifier (string)
  excludeFileList = {}, -- File patterns to exclude. (string list)
  releaseDirectory = nil, -- Where to store the project releases (string)
  }
end
